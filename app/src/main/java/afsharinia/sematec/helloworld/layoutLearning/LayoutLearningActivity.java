package afsharinia.sematec.helloworld.layoutLearning;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import afsharinia.sematec.helloworld.R;

public class LayoutLearningActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_layout_learning);
    }
}
