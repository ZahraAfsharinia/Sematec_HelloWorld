package afsharinia.sematec.helloworld.registration;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import afsharinia.sematec.helloworld.R;

public class RegistrationActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
    }
}
